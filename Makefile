.POSIX:

PREFIX = /usr/local
CC = gcc
HAXE = haxe


all: kraextract kracountframes

%: %_hlc/main.c
	$(LINK.c) -I$*_hlc $^ $(LOADLIBES) $(LDLIBS) -lhl -l:fmt.hdll -o $@

define build_hl
	$(HAXE) \
		--class-path src \
		--library format \
		--main $(<F) \
		--hl $@
endef

kracountframes_hlc/main.c: src/CountFrames.hx; $(build_hl)
kracountframes.hl: src/CountFrames.hx; $(build_hl)
kraextract_hlc/main.c: src/Extract.hx; $(build_hl)
kraextract.hl: src/Extract.hx; $(build_hl)

install: all
	mkdir -p $(DESTDIR)$(PREFIX)/bin
	cp -f kracountframes $(DESTDIR)$(PREFIX)/bin
	chmod 755 $(DESTDIR)$(PREFIX)/bin/kracountframes
	cp -f kraextract $(DESTDIR)$(PREFIX)/bin
	chmod 755 $(DESTDIR)$(PREFIX)/bin/kraextract

uninstall:
	rm -f $(DESTDIR)$(PREFIX)/bin/kracountframes
	rm -f $(DESTDIR)$(PREFIX)/bin/kraextract

clean:
	rm -fr kracountframes kracountframes.hl kracountframes_hlc
	rm -fr kraextract kraextract.hl kraextract_hlc

.PHONY: all clean install uninstall
