import sys.io.File;
import haxe.io.Path;
import haxe.zip.Entry;
import haxe.io.Bytes;
import haxe.Exception;

class Extract {
	public static function main() {
		final args = Sys.args();
                if (args.length != 4 && args.length != 2) {
                        showUsage();
                }
		if (args.length == 2 && (args[0] == '--frame' || args[1] == '--frame')) {
                        showUsage();
		}
		if (args.length == 4 && args[0] != '--frame') {
			showUsage();
		}
		final frame: Int = args.length == 4 ? Std.parseInt(args[1]) : 0;
		var input = args.length == 4 ? args[2] : args[0];
		var output = args.length == 4 ? args[3] : args[1];

		final image = new KritaImage(input, frame);
		final tempFile = tempFileName(output);
		File.saveBytes(tempFile, image.toPNG());
		sys.FileSystem.rename(tempFile, output);
	}

	static function showUsage() {
		Sys.stderr().writeString("Usage: kraextract [--frame n] input.kra output.png\n");
		Sys.exit(-1);
        }

	static function tempFileName(output: String): String {
		final path = Path.directory(output);
		return Path.join([path == "" ? "." : path, randomString(6) + ".tmp"]);
	}

	static function randomString(length:Int, ?charactersToUse = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"):String
	{
		var str = "";
		for (i in 0...length)
		{
			str += charactersToUse.charAt(randomInt(0, charactersToUse.length - 1));
		}
		return str;
	}

	static inline function randomInt(from:Int, to:Int):Int
	{
		return from + Math.floor(((to - from + 1) * Math.random()));
	}
}

class KritaImage {
	final entries: List<Entry>;
	final layers: Array<Layer> = [];
	final width: Int;
	final height: Int;
	final bytesPerPixel = 4;

	public function new(input: String, frame: Int) {
		final file = File.read(input);
		this.entries = new haxe.zip.Reader(file).read();
		final mainDoc = getEntry("maindoc.xml");	
		var xml = new haxe.xml.Access(Xml.parse(mainDoc.toString()).firstElement());
		var image = xml.node.IMAGE;
		if (image.att.colorspacename != "RGBA") {
			throw new Exception("Only RGBA images are supported.");
		}
		var name = image.att.name;
		width = Std.parseInt(image.att.width);
		height = Std.parseInt(image.att.height);
		for( layer in image.node.layers.nodes.layer ) {
			if (layer.att.nodetype != "paintlayer") continue;
			if (layer.att.visible != "1") continue;
			if (layer.att.colorspacename != "RGBA") {
				throw new Exception("Only RGBA images are supported.");
			}
			if (layer.att.compositeop != "normal") {
				throw new Exception("Only normal composite layers are supported.");
			}
			if (layer.att.compositeop != "normal") {
				throw new Exception("Only normal composite layers are supported.");
			}
			if (layer.att.opacity != "255") {
				throw new Exception("Only fully opaque layers are supported.");
			}
			final x = Std.parseInt(layer.att.x);
			final y = Std.parseInt(layer.att.y);
			final keyframe = layer.has.keyframes
				? KeyFrame.fromXml(getLayerEntry(name, layer.att.keyframes), frame)
				: new KeyFrame(layer.att.filename, x , y);
			layers.push(new Layer(getLayerEntry(name, keyframe.filename), keyframe.x, keyframe.y));
		}
		layers.reverse();
	}

	public function toPNG(?level = 9 ) {
		final pixels = compositeLayers();
                final png = std.format.png.Tools.build32BGRA(width, height, pixels #if (format >= "3.3") , level #end);
                final output = new haxe.io.BytesOutput();
                new format.png.Writer(output).write(png);
                return output.getBytes();
	}

	function compositeLayers(): Bytes {
		final pixels = Bytes.alloc(width * height * bytesPerPixel);
		final imageStride = width * bytesPerPixel;
		for (layer in layers) {
			final imageX = layer.x < 0 ? 0 : layer.x;
			final imageY = layer.y < 0 ? 0 : layer.y;
			final layerX = layer.x < 0 ? -layer.x : 0;
			final layerY = layer.y < 0 ? -layer.y : 0;
			final w = Std.int(Math.min(layer.width - layerX, width - imageX));
			final h = Std.int(Math.min(layer.height - layerY, height - imageY));
			final layerStride = layer.width * bytesPerPixel;
			for (y in 0...h) {
				final imageStart = (imageY + y) * imageStride;
				final layerStart = (layerY + y) * layerStride;
				for (x in 0...w) {
					final ip = imageStart + (imageX + x) * bytesPerPixel;
					final lp = layerStart + (layerX + x) * bytesPerPixel;
					final alphaL = layer.pixels.get(lp + 3) / 255;
					final alphaI = pixels.get(ip + 3) / 255;
					final alphaD = alphaL + alphaI * (1 - alphaL);
					pixels.set(ip + 3, Std.int(alphaD * 255));
					for (p in 0...3) {
						pixels.set(ip + p, Std.int((layer.pixels.get(lp + p) * alphaL + pixels.get(ip + p) * alphaI * (1 - alphaL)) / alphaD));
					}
				}
			}
		}
		return pixels;
	}

	function getEntry(name: String): Bytes {
		for (entry in entries) {
			if (entry.fileName == name) {
				haxe.zip.Tools.uncompress(entry);
				return entry.data;
			}
		}
		throw new Exception('No ${name} entry in file');
	}

	function getLayerEntry(base: String, name: String): Bytes {
		return getEntry(haxe.io.Path.join([base, "layers", name]));
	}
}

class KeyFrame {
	public final filename: String;
	public final x: Int;
	public final y: Int;

	public function new(filename: String, x: Int, y: Int) {
		this.filename = filename;
		this.x = x;
		this.y = y;
	}

	public static function fromXml(doc: Bytes, time: Int): KeyFrame {
		var xml = new haxe.xml.Access(Xml.parse(doc.toString()).firstElement());
		var keyframe: haxe.xml.Access = null;
		for(frame in xml.node.channel.nodes.keyframe) {
			if (Std.parseInt(frame.att.time) > time) {
				break;
			}
			keyframe = frame;
		}
		if (keyframe == null) {
			throw new Exception("No keyframe found");
		}
		final filename = keyframe.att.frame;
		final x = Std.parseInt(keyframe.node.offset.att.x);
		final y = Std.parseInt(keyframe.node.offset.att.y);

		return new KeyFrame(filename, x, y);
	}

	public function toString() {
		return 'KeyFrame($filename, $x, $y)';
	}
}

class Layer {
	public final width: Int;
	public final height: Int;
	public final x: Int;
	public final y: Int;
	public final pixels: Bytes;
	final data: Bytes;
	var cur = 0;

	public function new(data: Bytes, x: Int, y: Int) {
		this.data = data;

		final version = getField("VERSION");
		if (version != 2) {
			throw new Exception("Only VERSION 2 layers supported");
		}
		final tileWidth = getField("TILEWIDTH");
		final tileHeight = getField("TILEHEIGHT");
		final bytesPerPixel = getField("PIXELSIZE");
		final numTiles = getField("DATA");

		var top = 0;
		var bottom = 0;
		var left = 0;
		var right = 0;
		final tiles: Array<Tile> = [];
		for(i in 0...numTiles) {
			final tile = getTile();
			tiles.push(tile);
			top = Std.int(Math.min(top, tile.y));
			bottom = Std.int(Math.max(bottom, tile.y + tileHeight));
			left = Std.int(Math.min(left, tile.x));
			right = Std.int(Math.max(right, tile.x + tileWidth));
		}

		this.x = x + left;
		this.y = y + top;
		width = right - left;
		height = bottom - top;
		pixels = Bytes.alloc(width * height * bytesPerPixel);
		final layerStride = width * bytesPerPixel;
		final tileStride = tileWidth * bytesPerPixel;
		for(tile in tiles) {
			final tilePixels = tile.decompress(tileWidth, tileHeight, bytesPerPixel);
			final relative_top = tile.y - top;
			final relative_left = tile.x - left;
			for(y in 0...tileHeight) {
				pixels.blit((relative_top + y) * layerStride + relative_left * bytesPerPixel, tilePixels, y * tileStride, tileStride);
			}
		}
	}

	function getField(name: String): Int {
		final line = getLine();
		final parts = line.split(" ");
		if (parts.length != 2) {
			throw new Exception("Layer data malformed: not a valid field");
		}
		if (parts[0] != name) {
			throw new Exception('Layer data malformed: expecting "${name}" but found "${parts[0]}"');
		}
		return Std.parseInt(parts[1]);
	}

	function getLine(): String {
		var pos = cur;
		while( data.get(pos) != 0x0a ) {
			pos++;
		}
		final line = data.getString(cur, pos - cur);
		cur = pos + 1;
		return line;
	}

	function getTile(): Tile {
		final line = getLine();
		final parts = line.split(",");
		if (parts.length != 4) {
			throw new Exception("Layer data malformed: not a valid tile");
		}
		final x = Std.parseInt(parts[0]);
		final y = Std.parseInt(parts[1]);
		final length = Std.parseInt(parts[3]);
		final tile = new Tile(x, y, data.sub(cur, length));
		cur += length;
		return tile;
	}

	public function toString() {
		return 'Layer($x, $y, $width, $height)';
	}
}

class Tile {
	public final x: Int;
	public final y: Int;
	final data: Bytes;

	public function new(x: Int, y: Int, data: Bytes) {
		this.x = x;
		this.y = y;
		this.data = data;
	}

	public function decompress(width: Int, height: Int, bytesPerPixel: Int) {
		final decompressed = Bytes.alloc(width * height * bytesPerPixel);
		lzffDecompress(decompressed, data.sub(1, data.length - 1));
		final pixels = Bytes.alloc(width * height * bytesPerPixel);
		delinearizeColors(decompressed, pixels, bytesPerPixel);
		return pixels;
	}

	function lzffDecompress(output: Bytes, input: Bytes)
	{
		var ip = 0;
		var op = 0;
		var ref = 0;
		var ip_limit = input.length - 1;

		while (ip < ip_limit) {
			var ctrl = input.get(ip) + 1;
			var ofs = ((input.get(ip)) & 31) << 8;
			var len = (input.get(ip++)) >> 5;

			if (ctrl < 33) {
				/* literal copy */
				if (op + ctrl > output.length)
					return;

				/* crazy unrolling */
				if (ctrl > 0) {
					output.set(op++, input.get(ip++));
					ctrl--;

					if (ctrl > 0) {
						output.set(op++, input.get(ip++));
						ctrl--;

						if (ctrl > 0) {
							output.set(op++, input.get(ip++));
							ctrl--;

							while (ctrl > 0) {
								output.set(op++, input.get(ip++));
								ctrl--;
							}
						}
					}
				}
			} else {
				/* back reference */
				len--;
				ref = op - ofs;
				ref--;

				if (len == 7 - 1)
					len += input.get(ip++);

				ref -= input.get(ip++);

				if (op + len + 3 > output.length)
					return;

				if (ref < 0)
					return;

				output.set(op++, output.get(ref++));
				output.set(op++, output.get(ref++));
				output.set(op++, output.get(ref++));
				while (len > 0) {
					output.set(op++, output.get(ref++));
					len--;
				}
			}
		}
	}

	function delinearizeColors(input: Bytes, output: Bytes, pixelSize: Int)
	{
		var outputByte = 0;
		var lastByte = output.length - 1;

		var strideSize = Std.int(output.length / pixelSize);
		var startByte = 0;

		while (outputByte <= lastByte) {
			var inputByte = startByte;

			for(i in 0...pixelSize) {
				output.set(outputByte, input.get(inputByte));
				outputByte++;
				inputByte += strideSize;
			}

			startByte++;
		}
	}

	public function toString() {
		return 'Tile($x, $y)';
	}
}
