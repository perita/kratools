import sys.io.File;
import haxe.io.Input;
import haxe.io.Bytes;
import haxe.zip.Entry;
import haxe.zip.Reader;
import haxe.zip.Tools;
import haxe.xml.Access;

class CountFrames {
	public static function main() {
		final args = Sys.args();
                if (args.length != 1) {
                        showUsage();
                }
		var input = File.read(args[0]);
		var maindoc = findMaindoc(input);
		var xml = new Access(Xml.parse(maindoc.toString()).firstElement());
		if (hasKeyframes(xml.node.IMAGE.node.layers.nodes.layer)) {
			var range = xml.node.IMAGE.node.animation.node.range;
			Sys.println('${range.att.from} ${range.att.to}');
		} else {
			Sys.println("0 0");
		}
	}

	static function showUsage() {
                Sys.stderr().writeString("Usage: kracountframes input.kra\n");
                Sys.exit(-1);
        }

	static function findMaindoc(input: Input): Null<Bytes> {
		var entries = new Reader(input).read();
		for (entry in entries) {
			if (entry.fileName == "maindoc.xml") {
				Tools.uncompress(entry);
				return entry.data;
			}
		}
		return null;
	}

	static function hasKeyframes(layers: Array<Access>): Bool {
		for(layer in layers) {
			if (layer.has.keyframes) {
				return true;
			}
		}
		return false;
	}
}
